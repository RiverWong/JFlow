package bp.gpm;

import bp.da.*;
import bp.web.*;
import bp.en.*;
import java.util.*;

/** 
 岗位菜单
*/
public class StationMenuAttr
{
	/** 
	 菜单
	*/
	public static final String FK_Menu = "FK_Menu";
	/** 
	 岗位
	*/
	public static final String FK_Station = "FK_Station";
	/** 
	 是否选中.
	*/
	public static final String IsChecked = "IsChecked";
}